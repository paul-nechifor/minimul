<?php

$articole = array
(
    array
    (
        "2011-04-23 10:00",
        "<p>Asta-i un fel de jurnal (ce alții ar numi <em>blog</em>), numai că nu-mi impun limite artificiale și nici norme de producție.</p>",
        "<p>Mi se pare mai interesant formatul ăsta de exprimare pentru că nu trebuie să-l structurez excesiv și să-l îmbogățesc artificial. Ați observat că titlurile la articolele de pe jurnale nu sunt imporante? Ei bine, de asta eu nu pun titluri.</p>",
    ),
    array
    (
        "2011-04-23 11:50",
        "<p>Sunt mai ciudat pentru că sunt crescut în pădure, sub o piatră.</p>",
    ),
    array
    (
        "2011-04-23 15:27",
        "<p>Poți să vezi cum zboară alene fluturii prin fața ta când mergi cu mașina? Ei, cu bicicleta da. Eu sub zeci chilometrii nu pot să merg cu bicicleta... parcă n-are rost să mă obosesc pentru atâta.</p>",
    ),
    array
    (
        "2011-04-23 15:52",
        "<p>Ce mă oftică unitățile „imperiale“ când le aud prin documentare sau mai știu eu ce, dar mai ales când sunt prezente în România din inerție la dimensiunile monitoarelor spre exemplu.</p>",
    ),
    array
    (
        "2011-04-23 15:55",
        "<p>Dacă primești <em>letters</em> prin <em>mail</em> de ce se zice că primești <em>emails</em> prin <em>email</em>?</p>",
    ),
    array
    (
        "2011-04-24 14:47",
        "<p>Degetul mic și cel de lângă de la mâna dreaptă (și doar degetele asta) îmi amorțesc mereu când merg cu bicicleta și stau așa câteva zile bune. De ce?</p>",
        "<p>Am să-mi iau mănuși să văd dacă asta rezolvă problema. Și o să mă mai ajute la ceva. Ieri am fost cu bicicleta de la Iași la Podul Iloaiei și înapoi (vreo două ore) și aseară m-am trezit cu mâinile complet roșii de la sfârșitul degetelor până la încheietură. Bine că am mers cu geaca pe mine deși era cam cald. La soare reacționez mai rău ca un vampir, dar un vampir măcar are avantajul că știe imediat că îi este rău.</p>",
    ),
    array
    (
        "2011-04-24 16:03",
        "<p>Tușesc foarte tare. Am trecut peste punctul peste care doar tușesc, acum aș putea spune ca vomit aer. Sau cel puțin cu această impresie îi încânt pe oamenii din jur.</p>",
    ),
    array
    (
        "2011-04-24 16:01",
        "<p>Nu mă mai uit de mult la televizor și pentru știri de obicei intru pe Euronews ca să văd ce se mai întâmplă prin lumea asta.</p>",
    ),
    array
    (
        "2011-04-24 16:03",
        "<p>Ce cool e să combini english with romanian. Parol!</p>",
    ),
    array
    (
        "2011-04-24 23:00",
        "<p>Ce mă enervează cel mai tare la bloage este că nu există un fel de a pune un semn de carte universal ca să pot să știu unde am rămas. Bine, asta și faptul că sunt scrise pe dos. Adică dacă eu vreau să citesc un blog de pe care n-am intrat de vreo săptămână trebuie să dau mai în jos și să trec peste toate articolele pe care nu le-am citit și după ce am citit articolul curent trebuie să dau mai în sus și să mă opresc la titlu, dau în jos să citesc, iar în sus la următorul titlu (sau precedentul în ordinea afișării). Mai e rău că pe unele jurnale nu poți să citești tot acolo, trebuie să dai pe „<em>read more</em>“ (sau ceva de genul pentru că nu știu oamenii să-și bage jurnalu-n română dacă scriu în română).</p>",
        '<p>Nu ar fi mai bine dacă pur și simplu ar exista o opțiune să vezi tot jurnalul ca o pagină uriașă scrisă în ordine? Las semnul de carte, doar asta vreau. Citesc acum <a href="http://rarome.wordpress.com/">un jurnal</a> și o să-mi trebuiească sute de clicuri ca să citesc ce a scris în luna ianuarie. Ar fi mai ușor să citesc o carte. Tehnologia în loc să avanseze, îngreunează cititul.'
    ),
    array
    (
        "2011-04-25 00:03",
        "<p>Sunt așa de încurcat încât nu numai că nu știu cu ce să încep să ordonez, să fac, să dreg, dar nici nu știu pe ce calculator să fac lista sau de unde să iau un pix și-o foaie să scriu.</p>",
    ),
    array
    (
        "2011-04-26 01:15",
        '<p>Sumerienii în 3100 î.e.n. numai în Uruk aveau mai mult de 12 <a href="http://en.wikipedia.org/wiki/History_of_writing_ancient_numbers#Invention_of_tokens_for_record_keeping">sisteme numerice</a> pentru numărat. Adică pentru numărat animale foloseai un tip și pentru numărat grâu altul. În franceză și engleză o literă poate reprezenta mai multe sunete diferite sau chiar niciunul sau trebuie grupate ca să dea un sunet sau poate reprezenta mai multe sunete deodată. Acum, în schimb, mai tot globul înțelege numerele arabe și limba română se scrie ușor.',
        '<p>Ce vreau să zic prin asta este că cu timpul abandonăm complexitățile inutile pentru că doar folosind sisteme simple putem să construim altele mai complicate care să se bazeze pe ele. Nu-i de mirare că acei sumerieni n-aveau matematică și științe avansate și că francezilor le ia așa de mult timp să-și învețe copiii să scrie și să citească. În epoca asta oamenii au nevoie să învețe foarte multe lucruri deci nu putem avea o scriere complicată intenționat ca la egipteni care au făcut asta ca să-și mențină slujbele scribii („gioburi“ pentru oamenii din ziua de azi). În timp ce un englez memorează cele <a href="http://www.say-it-in-english.com/SpellHome.html">1100 de feluri</a> de a scrie cele 44 de sunete și care trebuie folosit pentru care cuvânt, un român învață imediat sunetele asociate literelor alfabetului și puținele excepții și poate să treacă la lucruri mai importante care-i vor folosi în viață. (Bine, că nu se întâmplă așa e vina sistemului de educație prost, dar ideea e că au capacitatea.)',
        "<p>Deci vă rog, haideți să abandonăm anglicismele sau măcar să le scriem fonetic. Dacă cuvântul „<em>iceberg</em>“ se scrie „aisberg“ în română (da, căutați în dicționare dacă nu știați asta) de ce nu vor oamenii să scrie „sait“ în loc de „<em>site</em>“? Mai corect ar fi să fie folosit cuvântul „sit“ care deja există în limba noastră, dar orice e mai bine decât „<em>site</em>“. Pun aceeași întrebare pentru toate miile de cuvinte care sunt băgate în limba română fără a fi trecute prin filtrul scrierii noastre. Ăștia aduc contrabandă în limba română și vameși se fac că nu văd. De ce toți au impresia că nu e <em>cool</em> (sau e <em>kool</em> acum) să mai scrii fonetic cuvintele importate recent?</p>",
        "<p>Acum văd oamenii care au abandonat cuvântul „blugi“ (de la „<em>blue jeans</em>“) pentru a scrie „<em>jeanși</em>“... Nici nu știu cum ar trebui să citesc corcitura asta. „Giinși“ oare? Dar nici nu înțeleg de ce s-a băgat „și“-ul ăla acolo în loc de „s“-ul din „jeans“... Dacă ar fi rămas scris ca în engleză puteam să zic că nu-i cuvânt românesc, nu-i al meu deci să și-l ia înapoi, dar „jeanși“ e cuvântul corcitură născut după ce „jeans“ a <strong>violat</strong> pluralul românesc și a abandonat-o (pe ea, plurarul românesc care e o doamnă tânără și foarte respectabilă în metafora asta) să crească mutatul ăsta. Doamna Plural nu crede că e corect să-și abandoneze mutanto-corcitura de copil și o înțelegem că, mamă fiind, nu vrea să facă asta, dar trebuie făcut ceva! Vameșii trebuie să-l oblige pe violator să... sau nu, statul trebuie să... Ă? E, m-am pierdut în metafore, dar ideea este că nu trebuie să folosim cuvântul ăsta. Mulțumesc.</p>",
    ),
    array
    (
        "2011-05-02 18:42",
        "<p>Cum Yahoo a decăzut așa de tare ar fi amuzat ca peste câțiva ani să se adeverească miile de <em>mass</em>-uri și să se închidă.</p>",
    ),
    array
    (
        "2011-05-02 19:01",
        "<p>Aș vrea să creez un sit care să fie echivalentul la TV Tropes pentru argumente care să includă argumentele la toate pozițiile și contraargument... și contraargumentele la contraargumente. Așa o să se sature lumea de vorbit în contradictoriu.</p>",
    ),
    array
    (
        "2011-05-02 19:04",
        '<p>Sunt impresionat de oamenii care nu renunță la ce cred, chiar dacă-s inspirați de religie. Andrei Ciurunga a fost un poet basarabean care s-a mutat în Brăilea după ce a fost ocupată Basarabia și a făcut ani grei de pușcărie pentru poeziile scrise. Poezia lui numită <em><a href="http://www.cerculpoetilor.net/Nu-s-vinovat-fata-de-tara-mea_Andrei-Ciurunga.html">Nu-s vinovat față de țara mea</a></em> e <a href="http://www.youtube.com/watch?v=xJRaT2K4urA#t=74s">cântată de Tudor Gheorghe</a> și așa am auzit e el.'
    ),
    array
    (
        "2011-05-03 21:09",
        "<p>Îmi verific poșta electronică de la Yahoo de fiecare dată cu panică să nu se fi șters, asta din cauză că după câteva luni (perioada de timp în care îmi amintesc eu că trebuie s-o verific), dacă nu mai intrii pe el ți-o șterg dar eu n-o mai folosesc pe aia de mult... oricum e plină de mii de mesaje de la Facebook și mi-ar fi greu să găsesc mesaje importante.</p>",
    ),
    array
    (
        "2011-05-04 15:40",
        "<p>Dacă nu zicem „ca calul“ pentru ca e urât să zici caca, de ce nimeni n-are nicio problemă cu cuvântul manipulare?</p>",
    ),
    array
    (
        "2011-05-04 15:48",
        "<p>Nu-mi plac persoanele care se află într-un ciclu neîntrerupt cu două stări: ofensare celorlalți și victimizare proprie pentru răspunsul legitim al celor ofensați.</p>",
    ),
    array
    (
        "2011-05-04 19:25",
        "<p>Toate filmele au o realitate internă care trebuie să fie credibilă independent de realitatea adevărată. Adică într-un film cu vampiri presupui că sunt reali și că au caracteristicile din filmul respectiv, nu din alte opere de ficțiune și nu-ți zici mereu că nu există pentru că așa-i în realitate.</p>",
        "<p>Dar sunt momente care distrug abilitatea de a te menține în film. În filmul <em>The Prestige</em>, Tesla trebuie să inventeze o mașină care transportă oamenii instantaneu, dar inventează altceva: o mașină care duplică ce are înauntru. Pentru că e film, acceptăm așa ceva și vedem ce se întâmplă mai departe. Filmul e bun, dar faza asta îl cam distruge pentru că e clar ce ar face orice om care ar avea asemenea mașină: ar băga tot aurul care-l are și-l înmulțește de o mie de ori apoi cumpăra diamante de banii ăia și vinde diamante la kg. Dar filmul merge mai departe ca și cum nimeni nu se gândește la asta.</p>",
    ),
    array
    (
        "2011-05-05 12:44",
        "<p>Nimeni nu-și schimbă opinia pentru că este jignit.</p>",
    ),
    array
    (
        "2011-05-05 12:48",
        "<p>Nu cred că e o idee bună să critic pe cineva care face ceva ce eu consider prost dacă nu strică. Dacă unii au prea mult timp la dispoziție trebuie să-l umple cu ceva.</p>",
    ),
    array
    (
        "2011-05-06 15:04",
        "<p>Titlul filmului <em>Dog Day Afternoon</em> (un film bun din anii 70) se referă la <em>dog days</em>, zilele cele mai calde din vară. Denumirea asta provine din latină de la <em>diēs caniculārēs</em>. Tot de aici au francezii denumirea de <em>canicule</em> și de la ei noi avem „caniculă“. Nu mi-ar fi trecut niciodată prin minte că acest cuvânt are vreo legătură cu câinii.</p>",
    ),
    array
    (
        "2011-05-06 23:14",
        "<p>Cică Esperanto se învață <em>foarte</em> ușor. L-aș învăța și eu ca să mă laud ca știu trei limbi. Dar stau foarte prost cu memoria și probail că pentru mine ar fi mult mai greu.</p>",
    ),
    array
    (
        "2011-05-08 19:20",
        "<p>Azi am fost cu bicileta până după Târgu Frumos ca să fac o sută de chilometrii cu întoarcerea. E fain să mergi primăvara și să dai de copaci înfloriți și de mirosul lor. Dar unele mirosuri nu sunt limitate anumitor anotimpuri și mă refer la mirosul de balegă proaspătă (deși eu merg pe drumuri mari se bagă și nenorociții cu caii lor pe acolo) și al fabricilor sau crescătoriilor din jur. La nu mulți chilometrii de la ieșirea din Iași este un semn pe care scrie mare „PUI ZBURAȚI“. Miroare ca dracu de acolo! Se întâmplă să ajungă mirosul ăla până la dealul din apropiere și trebuie să urc, săracul de mine, dealul cu mirosul ăla. (Mai e și faza că ce dracu înseamnă „pui zburați“.) Văd și turme de oi și vaci, dar de cele mai multe ori nu mi-a venit un miros neplăcut, probabil sunt prea depărtate.</p>",
        "<p>Altă chestie care nu-mi place când merg prin afara orașului este faptul că țăranii nu știu pe ce parte se merge cu bicileta! La satele astea care au trotuar îi vezi pe acolo cu bicicletele ca și în oraș și-mi vine să țip la ei să vină lângă mine pe stradă că pe aici se merge că doar nu suntem pietoni și dacă am merge toți pe stradă s-ar obișnui „mașiniștii“ cu noi și probabil n-ar mai arunca oamenii cu sticle goale în mine, cum a făcut unul azi sau să ragă ca o vită nebună cu gura spartă la mine cum e mai comun. Dar oricum, ce spuneam eu este că văd țărani care merg pe stânga, de parcă ar fi pietoni pe o strada care n-are trotuar! Îmi place faptul că nu trebuie să ai nimic ca să mergi cu bicileta, dar dacă unii chiar nu știu pe ce parte a străzii se merge poate că ar trebui un curs obligatoriu înainte să poți să folosești o bicicletă. Cât despre rahat, cred că trebuie obligați cei cu căruțele să-l culeagă. E balega calului tău deci ia-o acasă cu tine.</p>",
        "<p>Defapt nu știu de ce mă leg de țarani, că am văzut și în Iași oameni care merg pe stânga.</p>",
    ),
    array
    (
        "2011-05-15 23:08",
        "<p>Conform <a href='http://en.wikipedia.org/wiki/Law_of_Large_Numbers'>legii numerelor mari</a> media rezultatelor obținute din încercări succesive este foarte aproape de valoarea așteptată (din punct de vedere probabilistic) după un număr mare de încercări. Cam așa stă și <em>karma</em>. Dacă ți se întâmplă ceva rău o să urmeze și ceva bun ca să se echilibreze. Dar este o <a href='http://en.wikipedia.org/wiki/Gambler%27s_fallacy'>eroare</a> să crezi că se modifică șansele.</p>",
    ),
    array
    (
        "2011-05-16 00:06",
        "<p><a href='http://www.the-digital-picture.com/Reviews/Canon-EF-1200mm-f-5.6-L-USM-Lens-Review.aspx'>Ăsta da obiectiv</a>! Are 16,5kg și recent, unul la mâna a doua, s-a vândut cu 120.000 de dolari. Se numește Canon EF 1200mm f/5.6L USM. Râd ca un prost că în <a href='http://www.youtube.com/watch?v=U0E-nZOlY_k'>video</a> are cureaua Canon pusă, de parcă ar putea ține așa ceva la gât!</p>",
    ),
    array
    (
        "2011-05-16 15:01",
        "<p>Mac-urile sunt PC-uri. Apple și fanaticii încercă să mențină mentalitatea de ei vs. noi și au nevoie de o denumire ca să se diferențieze. Dar denumirile nu au sens pentru că <strong>calculatoarele făcute de Apple sunt calculatoare personale</strong>. Diferențierea adevărată pe care are trebui să insiste ei este cea de Mac OS vs. Windows, adică pe sisteme de operare nu pe <em>hardware</em>.</p>",
        "<p>Apple a fabricat PC-uri înainte de IBM încă din 1976. Când IBM a intrat pe piață microcalcualtoarelor în 1981 au denumit calculatorul lor IBM PC. Termenul de „calculator personal“ se folosea încă din 1972. IBM PC-urile au avut mare succes în parte pentru că puteau fi ușor modificate și s-au făcut multe clone. Calculatorul pe care-l folosești acum cel mai probabil este compatibil IBM PC, dar probabil nu e făcut de IBM (nu același lucru poți spune despre calculatoarele Apple). De câțiva ani, nu numai că nu poți spune că Mac-urile nu sunt calcualtoare personale, dar nici nu mai poți spune că nu sunt compatibile IBM PC. Adică poți rula Windows pe Mac-uri și Mac OS pe calculatoare care nu-s făcute de Apple (deși e mai greu).</p>",
        "<p>Ce nu-mi place mie personal la treaba asta este că se face o asociere falsă între calculatoare care nu-s făcute de Apple și calculatoare care rulează Windows, de parcă nu ar fi alte sisteme de operare. Eu folosesc Arch Linux pe <em>hardware</em> non-Apple pentru că calculatoarele făcute de Apple sunt mereu mai scumpe... mult mai scumpe. Mac OS-ul este un fel de UNIX, la fel ca Linux, dar nu l-aș folosi pentru că e mult prea controlat. Cu Linux ai libertate, pe lângă faptul că nu costă nimic.</p>",
    ),
    array
    (
        "2011-05-16 00:06",
        "<p>La români, numele de familie este mereu ultimul! Obiceiul de scrie numele de familie primul este un viciu comunisto-birocrat. Mai rău este când cineva chiar își spune numele așa:</p>",
        "<p>—&nbsp;Mă cheamă Popescu Vlad.</p>",
        "<p>—&nbsp;Nu, prostane, îi răspund eu în imaginația mea, te cheamă <em>Vlad Popescu</em>!</p>",
        "<p>Popoarele estice (japonezii, chinezii, ungurii, indienii ș.a.m.d.) își pun numele de familie primul. Așa e la ei de sute de ani, dar la noi e un obicei leneș de pe timpul când nu puteau oamenii să sorteze numele pe calculatoare.</p>",
    ),
    array
    (
        "2011-05-21 09:27",
        "<p>La țară nu doar bicicletele merg pe invers, ci și meșinile. Ieri când eram în Movileni și mergeam spre Iași am văzut că o mașină trage pe stânga lui, jumate pe drum și jumate pe pământ, și contunuă așa. Omul prost nici măcar nu-mi dădea voie să-l „depășesc“ pe arătură (cum fac câteodată ca să nu mă complic și pentru că-i făcută bicicleta pentru așa ceva) și a trebuit să mă duc eu cu bicicleta pe stânga. Ce m-a enervat mai tare a fost faptul că <em>el</em> era enervat. Am răcnit la el „Nu știi pe ce parte se merge?“ și mi-am continuat drumul. După câțiva chilometrii, la deal, am văzut o mașină „parcată“ perpendicular cu strada pe portiera din dreapta. Sper să i se întâmple și lui la fel, că nu-și merită permisul ăștia.</p>",
    ),
    array
    (
        "2011-05-21 09:27",
        "<p>Oare toate numele feminine românești se termină în <em>a</em>? Făcând o căutare rapidă cu Python într-o bază de date de telefoane am găsit patru exemple care nu se termină cu <em>a</em>: Carmen, Lili, Zoe și Iris. În orice caz, sunt <em>foarte</em> puține.</p>",
        "<p>Înainte de a avea discuția asta trebuie hotărât ce este un nume românesc. Un purist extrem ar zice că nici Maria nu-i nume românesc pentru că e de origine ebraică și se găsește în foarte țări, dar eu consider un nume românesc acela care e destul de întâlnit la români și, cel mai important, se scrie românește. Spre exemplu am mai găsit și Alice și Beatrice, dar nu le iau în considerare. La bărbați, Jean și Henry (printre altele) nu-s românești.</p>",
    )
);
?>
